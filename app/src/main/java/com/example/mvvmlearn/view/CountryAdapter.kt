package com.example.mvvmlearn.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.example.mvvmlearn.R
import com.example.mvvmlearn.model.Country
import com.example.mvvmlearn.utils.getProgressDrawable
import com.example.mvvmlearn.utils.loadImage
import kotlinx.android.synthetic.main.item_country.view.*

class CountryAdapter (val countries : MutableList<Country>) : RecyclerView.Adapter<CountryAdapter.CountryViewHolder>() {

    private var context : Context?= null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder  {
        context = parent.context
        val layoutInflate = LayoutInflater.from(context).inflate(R.layout.item_country, parent, false)
        return CountryViewHolder(layoutInflate)

    }

    override fun getItemCount(): Int = countries.size

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        holder.bind(countries[position])
    }

    fun updateCountries(countryUpdate: List<Country>) {
        this.countries.clear()
        this.countries.addAll(countryUpdate)
        notifyDataSetChanged()

    }

    class CountryViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
        private val countryName : TextView = itemView.name
        private val countryCapital: TextView = itemView.capital
        private val countryFlag : ImageView = itemView.image
        private val progressDrawable: CircularProgressDrawable = getProgressDrawable(itemView.context)
        fun bind(country: Country) {
            country.countryName.let {
                countryName.text = it
            }
            country.capital.let {
                countryCapital.text = it
            }
            country.flag.let {
                countryFlag.loadImage(it, progressDrawable)
            }


        }

    }
}