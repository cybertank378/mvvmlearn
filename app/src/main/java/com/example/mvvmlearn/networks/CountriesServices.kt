package com.example.mvvmlearn.networks


import com.example.mvvmlearn.di.DaggerApiComponent
import com.example.mvvmlearn.model.Country
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class CountriesServices {

    @Inject
    lateinit var api : CountriesApi
    /**
     *  Remove Services API Into Dagger 2 Dependency Injection
     */
    init {
        DaggerApiComponent.create().inject(this)
    }
    fun getCountries() : Single<List<Country>>{
        return api.getCountries()
    }
}