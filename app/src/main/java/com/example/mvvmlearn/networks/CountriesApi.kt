package com.example.mvvmlearn.networks

import com.example.mvvmlearn.model.Country
import com.example.mvvmlearn.networks.EndPoint.Countries
import io.reactivex.Single
import retrofit2.http.GET

interface CountriesApi {
    @GET(Countries)
    fun getCountries() : Single<List<Country>>
}