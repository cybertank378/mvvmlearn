package com.example.mvvmlearn.model

import com.google.gson.annotations.SerializedName

data class Masjid (
    @SerializedName("id") 
    val id : Int?,
    @SerializedName("kabkota") 
    val kabkota : String?,
    @SerializedName("kecamatan") 
    val kecamatan : String?,
    @SerializedName("nama_masjid")
    val nama : String?,
    @SerializedName("tipologi") 
    val tipologi : String?,
    @SerializedName("alamat")
    val alamat : String?,
    @SerializedName("lat") 
    val lat : Double?,
    @SerializedName("long") 
    val long : Double?

)
