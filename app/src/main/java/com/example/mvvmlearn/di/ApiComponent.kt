package com.example.mvvmlearn.di

import androidx.lifecycle.ViewModel
import com.example.mvvmlearn.networks.CountriesServices
import com.example.mvvmlearn.viewmodel.ListViewModel
import dagger.Component

@Component(modules = [ApiModule::class])
interface ApiComponent {
    fun inject(Service : CountriesServices)
    fun inject(viewModel: ListViewModel)
}