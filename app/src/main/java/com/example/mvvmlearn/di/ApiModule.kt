package com.example.mvvmlearn.di

import com.example.mvvmlearn.networks.CountriesApi
import com.example.mvvmlearn.networks.CountriesServices
import com.example.mvvmlearn.networks.EndPoint
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class ApiModule {

    @Provides
    fun provideCountriesApi() : CountriesApi{
        return Retrofit.Builder()
            .baseUrl(EndPoint.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(CountriesApi::class.java)
    }


    @Provides
    fun provideCountryService() : CountriesServices{
        return CountriesServices()
    }
}