package com.example.mvvmlearn

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.mvvmlearn.model.Country
import com.example.mvvmlearn.networks.CountriesServices
import com.example.mvvmlearn.viewmodel.ListViewModel
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

class ListViewModuleTest{
    @get:Rule
    var rule = InstantTaskExecutorRule()

    @Mock
    lateinit var countriesServices: CountriesServices

    @InjectMocks
    var listViewModel = ListViewModel()

    private var testSingle : Single<List<Country>>? = null

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
    }

    /**
     * For Test Businness Module If Success
     */
    @Test
    fun getCountriesSuccess(){
        val country = Country("countryName","capital", "url")
        val countriesList = mutableListOf(country)

        testSingle = Single.just(countriesList)
        `when`(countriesServices.getCountries()).thenReturn(testSingle)

        listViewModel.refresh()
        Assert.assertEquals(1, listViewModel.countries.value?.size)
        Assert.assertEquals(false, listViewModel.countryLoadError.value)
        Assert.assertEquals(false, listViewModel.loading.value)
    }

    /**
     * For Test Businness Module If Error
     */
    @Test
    fun getCountriesFailed(){
        testSingle = Single.error(Throwable())

        `when`(countriesServices.getCountries()).thenReturn(testSingle)
        listViewModel.refresh()
        Assert.assertEquals(true, listViewModel.countryLoadError.value)
        Assert.assertEquals(false, listViewModel.loading.value)

    }
    @Before
    fun setUpRxScheduler(){
        val immidiate = object : Scheduler(){

            override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
                return super.scheduleDirect(run, 0, unit)
            }
            override fun createWorker(): Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() }, false)
            }

        }

        RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immidiate}
        RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immidiate }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immidiate }
        RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immidiate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immidiate }
    }
}